
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

gsap.to('.letters-left', {
  xPercent: -150,
  ease: "none",
  scrollTrigger: {
    trigger: ".letters-left",
    start: "top center",
    end: "bottom top",
    scrub: true
  }
})