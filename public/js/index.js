/*Fixed Navbar*/

window.onscroll = () => { fixNavbar() };

const header = document.querySelector('nav');

const sticky = header.offsetTop;

function fixNavbar() {
  if (window.pageYOffset > sticky) {
    header.classList.add('sticky');
    header.style.opacity = "0.7";
  } else {
    header.classList.remove("sticky");
    header.style.opacity = "1";
  }
}

/*Reveal elements*/

const ratio = .1;

const options = {
  root: null,
  rootMargin: '0px',
  treshold: ratio
}

const handleIntersect = function (entries, observer) {
  entries.forEach(function (entry) {
    if (entry.intersectionRatio > ratio) {
      entry.target.classList.add('reveal-visible')
      observer.unobserve(entry.target)
    }
  })
}

const observer = new IntersectionObserver(handleIntersect, options);
observer.observe(document.querySelector('.reveal'))

/*ScrollReveal*/
var slideUp = {
  distance: '150%',
  origin: 'bottom',
  opacity: null,
  duration: 500,
  easing: 'cubic-bezier(.47,.44,.2,1.09)',
  opacity: 0.8
};
ScrollReveal().reveal('.reveal', slideUp)


/*Adding date*/
let now = new Date().getFullYear().toString().substr(-2);
let date = document.querySelector('.date');
date.textContent += now;
