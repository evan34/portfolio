const form = document.querySelector('form');

form.addEventListener('submit', async e => {
  e.preventDefault();
  const formData = new FormData(form);
  let data = Object.fromEntries(formData.entries());
  try {
    const json = JSON.stringify(data);
    const response = await fetch('/contact', {
      method: 'POST',
      body: json,
      headers: {
        "Content-Type": "application/json"
      }
    });
    const body = await response.status;
    const responseMessage = document.querySelector('#responseMessage');
    if (body === 200) {
      responseMessage.innerHTML = "<p>Email envoyé !</p>"
      document.querySelector('#name').value = '';
      document.querySelector('#email').value = '';
      document.querySelector('#message').value = '';
    }
  } catch (e) {
    responseMessage.innerHTML = "<p>Un problème est survenu !</p>"
  }
});
