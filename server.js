const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
require('dotenv').config()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static('public'));
app.use(express.json());

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public');
});

app.post('/contact', (req, res) => {
  console.log(req.body);
  const prodTransporter = nodemailer.createTransport({
    host: "mail.evanmartinez.fr",
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD
    }
  });

  let textBody = `DE: ${req.body.name} EMAIL: ${req.body.email} MESSAGE: ${req.body.message}`;
  let htmlBody = `<h2>Mail du formulaire de contact</h2><p>de: ${req.body.name} <a href="mailto:${req.body.email}">${req.body.email}</a></p><p>${req.body.message}</p>`;
  let mail = {
    from: "contact@evanmartinez.fr",
    to: "contact@evanmartinez.fr",
    subject: "Mail du formulaire de contact",
    text: textBody,
    html: htmlBody
  };

  prodTransporter.sendMail(mail, function (err, info) {
    if (err) {
      console.log(err);
      res.send('Erreur');
    }
    else {
      console.log('Email envoyé: ' + info.response);
      res.send('Succés');
    }
  });

})



const PORT = process.env.PORT || 3000;
app.listen(PORT, function (req, res) {
  console.log('Server is running at port: ', PORT);
});